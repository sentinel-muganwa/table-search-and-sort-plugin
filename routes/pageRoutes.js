require('dotenv').config()
const express = require('express')
const router = express.Router()

const { SITE_NAME } = process.env

/* GET home page. */
router.get('/', (req, res, next) => {
    res.render('unauthenticated/home', {
        'site-title': 'Sentinel Framework',
        'site-name': SITE_NAME,
        layout: 'unauthenticated/basic',
    })
})

/* 404. */
router.get('*', (req, res, next) => {
    res.render('common/404', { 'site-title': 'Sentinel Framework', 'site-name': SITE_NAME })
})

module.exports = router