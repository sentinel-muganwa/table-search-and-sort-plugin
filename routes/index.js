'use strict'
const apiRoutes = require('./apiRoutes')
const pageRoutes = require('./pageRoutes')

module.exports = {
    apiRoutes,
    pageRoutes,
}
