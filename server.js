/* eslint-disable no-undef */
require("dotenv").config(); // read .env files
const express = require("express");
const hbs = require("express-handlebars");
const logger = require('morgan')

// Import busboy
const busboy = require("connect-busboy");

const session = require('express-session');
const KnexStore = require('connect-session-knex')(session);

const db = require('./db');
const sessionStore = new KnexStore({ knex: db });

/** Project resources */
const helpers = require('./helpers')
const { normalizePort } = require('./helpers/functions')
const { apiRoutes, pageRoutes } = require('./routes');
const cookieParser = require("cookie-parser");

const app = express();

// Reload middleware setup during dev
if (process.env.NODE_ENV === 'development') {

    const livereload = require("livereload");
    const connectLiveReload = require("connect-livereload");

    const liveReloadServer = livereload.createServer();
    liveReloadServer.server.once("connection", () => {
        setTimeout(() => {
            liveReloadServer.refresh("/");
        }, 100);
    });

    app.use(connectLiveReload());
}

app.use(
    session({
        secret: process.env.SESSION_SECRET || 'sent-ag-654321',
        store: sessionStore,
        resave: false,
        saveUninitialized: false,
    })
)

const port = normalizePort(process.env.PORT || '3000')

//Setup handlebars templating
app.engine(
    'hbs',
    hbs({
        extname: 'hbs',
        defaultLayout: 'index',
        // eslint-disable-next-line no-undef
        layoutsDir: `${__dirname}/views/layouts/`,
        partialsDir: `${__dirname}/views/partials/`,
        helpers,
    })
)

app.set('view engine', 'hbs')
app.set('views', `${__dirname}/views`)

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

// Set public folder as root
app.use(express.static("public"));
app.use("/dataStore", express.static(__dirname + "/dataStore"));

// Allow front-end access to node_modules folder
app.use("/scripts", express.static(`${__dirname}/node_modules/`));

app.use(
    busboy({
        highWaterMark: 2 * 1024 * 1024, // Set 2MiB buffer
    })
); // Insert the busboy middle-ware

/** Setup routes */
app.use('/api/v1/', apiRoutes)
app.use('/', pageRoutes)

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404))
})

// Listen for HTTP requests on port 3000
app.listen(port, () => {
    console.log("listening on %d", port);
});

module.exports = app