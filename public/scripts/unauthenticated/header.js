(($, document, window) => {
    function underLineCurrentRoute($pathLink, pathname) {
        /** Initial link status */
        if (pathname === '/') {
            $('li').find('#index-link').addClass('active');
            $pageLink = $('#index-link');
        }
        else {
            const id = pathname.replace('/', '');
            $pageLink = $('li').find(`#${id}-link`);
            if ($pageLink.parent().hasClass('first')) {
                $pageLink.addClass('active');
            } else {
                const $firstUrlPart = id.split('-')[0];
                const $thisLink = $(`#${$firstUrlPart}-link`);
                $pageLink = $thisLink;
                $thisLink.addClass('active');
            }
        }
    }
    if (document && window) {
        /** Desktop Nav */
        const pathname = window.location.pathname;
        let $pageLink;
        underLineCurrentRoute($pageLink, pathname);
        /** Initial link status */
        if (pathname === '/') {
            $('li').find('#index-link').addClass('active');
            $pageLink = $('#index-link');
        }
        else {
            const id = pathname.replace('/', '');
            $pageLink = $('li').find(`#${id}-link`);
            if ($pageLink.parent().hasClass('first')) {
                $pageLink.addClass('active');
            } else {
                const $firstUrlPart = id.split('-')[0];
                const $thisLink = $(`#${$firstUrlPart}-link`);
                $pageLink = $thisLink;
                $thisLink.addClass('active');
            }
        }
        $('li.first').on('mouseenter', e => {
            const $el = $(e.target);
            $el.find('a').addClass('active');
        });
        $('li.first').on('mouseleave', e => {
            const $el = $(e.target);
            const $id = pathname.replace('/', '');
            const $currentLink = $el.find('a');
            const $grandParent = $el.parent().parent();
            const $grandParentId = $grandParent.attr('id');
            const $grandParentIdArr = $grandParentId && $grandParentId.split('-');
            const $idArr = $id.split('-');

            if ((($grandParentIdArr && ($idArr[0] !== $grandParentIdArr[0])) || !$grandParentIdArr) && $pageLink[0] !== $currentLink[0]) {
                $currentLink.removeClass('active');
                $grandParent.find('a').removeClass('active');
                underLineCurrentRoute($pageLink, pathname);
            }
        });

        $(document).on('click', (e) => {
            /** Mobile Nav */
            const $target = $(e.target);

            if ($target.attr('id') !== 'hamburger-menu' && $target.attr('id') !== 'dropdown-link' && $('#vertical-menu').css('display') === 'flex')
                $('#vertical-menu').css('display', 'none');

            if ($target.attr('id') === 'dropdown-link')
                $('#dropdown.mobi').css('display', 'flex');


        });
        $('#hamburger-menu').on('click', (e) => {
            e.stopPropagation();
            $('#vertical-menu').css('display', 'flex');
        });
    }
})(window.jQuery, document, window);