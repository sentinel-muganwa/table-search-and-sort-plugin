// Update with your config settings.
require("dotenv").config();
const { DB_HOST, DB, DB_USER, DB_PASSWORD } = process.env;

module.exports = {

    development: {
        client: 'mysql',
        connection: {
            host: DB_HOST,
            database: DB,
            user: DB_USER,
            password: DB_PASSWORD
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    },

    staging: {
        client: 'mysql',
        connection: {
            host: DB_HOST,
            database: DB,
            user: DB_USER,
            password: DB_PASSWORD
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    },

    production: {
        client: 'mysql',
        connection: {
            host: DB_HOST,
            database: DB,
            user: DB_USER,
            password: DB_PASSWORD
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    }

};
