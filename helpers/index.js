const ifOneOf = (v1, v2, options) => {
    if (v1 || v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifEitherFirstTrueOrSecondFalse = (v1, v2, options) => {
    if (v1 || !v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifEitherFirstFalseOrSecondTrue = (v1, v2, options) => {
    if (v1 || !v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifBoth = (v1, v2, options) => {
    if (v1 && v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifNeither = (v1, v2, options) => {
    if (!v1 && !v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifFirstTrueSecondFalse = (v1, v2, options) => {
    if (v1 && !v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifFirstFalseSecondTrue = (v1, v2, options) => {
    if (!v1 && v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifEqual = (v1, v2, options) => {
    if (v1 === v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

const ifNotEqual = (v1, v2, options) => {
    if (v1 !== v2) {
        return options.fn(this)
    }
    return options.inverse(this)
}

module.exports = {
    ifOneOf,
    ifBoth,
    ifNeither,
    ifFirstTrueSecondFalse,
    ifFirstFalseSecondTrue,
    ifEitherFirstFalseOrSecondTrue,
    ifEitherFirstTrueOrSecondFalse,
    ifEqual,
    ifNotEqual,
}
